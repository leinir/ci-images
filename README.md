# CI Images

This repository includes the container images used by KDE CI.

These are not expected to be used by developers directly; they are used to create
[Gitlab Templates](https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/gitlab-templates),
which lets developers create pipelines for building, testing and deploying
their apps and websites with a simple include file. If you need to add new
shared dependencies in the default templates, this is the right repository.

To learn more about how KDE uses Gitlab CI, see the wiki page
[Continuous Integration System](https://community.kde.org/Infrastructure/Continuous_Integration_System).
