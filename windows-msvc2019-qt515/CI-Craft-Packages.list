[Core]
# For multi-thread compilation
dev-utils/jom
# For general KDE compilation
dev-utils/cmake
# For everything that uses MIME information
libs/shared-mime-info
# Everything wants Qt 5
libs/qt5
# And some things want QtWebEngine
libs/qt5/qtwebengine
# Everything in Frameworks also needs Doxygen for QCH generation
dev-utils/doxygen

[Frameworks]
# ECM
dev-utils/icoutils

# KArchive
libs/libarchive
libs/liblzma

# KI18n
libs/gettext
data/iso-codes

# Solid
dev-utils/flexbison

# Prison
libs/qrencode
libs/libdmtx
libs/zxing-cpp

# KDocTools
libs/libxslt
libs/libxml2
data/docbook-dtd
data/docbook-xsl
perl-modules/uri-url
perl-modules/xml-parser

# KNotification
dev-utils/snoretoast

# KWallet
libs/gcrypt

# KJS
libs/pcre

# KHTML
libs/libpng
libs/libjpeg-turbo
libs/giflib

# KActivities
libs/boost/boost-headers

# KCodecs
dev-utils/gperf

# Purpose
kdesupport/qca

# Breeze Icons
python-modules/lxml

[Applications]
# libkexiv2
libs/exiv2

# libkdcraw
libs/libraw

# KStars
libs/eigen3
libs/wcslib
libs/libxisf
libs/libnova
libs/cfitsio

# Minuet
libs/glib
libs/fluidsynth

# KCalCore
libs/libical

# Analitza
libs/glew

# Okular
qt-libs/poppler
libs/ebook-tools
libs/chm
libs/discount
libs/djvu
libs/libspectre

# Kiten
libs/mman

# Lokalize
libs/hunspell

# Codevis
qt-libs/quazip
libs/zlib
libs/llvm
libs/catch2

# Filelight
kdesupport/kdewin

[Extragear]
# KDevelop
libs/llvm
kdesupport/grantlee

# Labplot
libs/gsl

# Skrooge
dev-utils/pkg-config

# KMyMoney & Alkimia
libs/libgmp
libs/mpfr
libs/sqlcipher
libs/libofx

# Amarok
libs/taglib

# Elisa
binary/vlc

# Calligra
libs/boost/boost-system

# Digikam
libs/sqlite
libs/x265
libs/tiff
libs/expat
libs/ffmpeg
libs/lcms2
libs/opencv
libs/lensfun
libs/openal-soft
libs/pthreads
libs/libjpeg-turbo

# NeoChat, Ruqola, ...
qt-libs/qtkeychain

# NeoChat
qt-libs/libquotient
qt-libs/qcoro
libs/cmark

# RKWard
binary/r-base

# libqgit2
libs/libgit2

# Glaxnimate
libs/potrace
